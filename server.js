console.log("");
console.log("//************************* MYVCARD-NODE App 2.0.0 **************************//");
console.log("Server Start Date : ", new Date());

// .env file read
import dotenv from 'dotenv'
// dotenv.config({ debug: process.env.DEBUG });
dotenv.config();

// Import Config
import config from './lib/config/index.js'

//import route
import route from './lib/route/index.js'

//import package
import express from "express"
import path from 'path'

//define _dirname
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

import loggerh from './lib/logger/index.js'
const logger = loggerh.logger

// config.dbConfig()
config.dbConfig.connectDb(config.cfg, (error) => {
    if (error) {
        logger.error(error, 'Exiting the app.');
        return error;
    }
    // init express app
    const app = express();

    // config express
    config.expressConfig(app, config.cfg.environment);

    //use route 
    route(app)

    // set the view engine to ejs
    app.set('views', __dirname + '/views');
    app.set('view engine', 'ejs');

    // set server home directory
    app.locals.rootDir = __dirname;

    // start server
    app.listen(config.cfg.port, () => {
        logger.info(`Express server listening on ${config.cfg.ip}:${config.cfg.port}, in ${config.cfg.TAG} mode`);
    });
});