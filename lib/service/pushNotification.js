import FCM from 'fcm-node'
import loggerh from '../logger/index.js';
const logger = loggerh.logger
import config from '../config/index.js'

var fcmServerKey = config.cfg.fcmServerKey; //put your server key here
var fcm = new FCM(fcmServerKey);

// const deviceIds = 'duLFtTVlTMGH8ZyKvLSAkj:APA91bExG6-KLQXDZknAXwcozHER3wiwprl-MGyJhi8KOf56PlQJPY9NbpHVe_Xyrm0SFgvWx6YwEA43gDtCC6MTOn-aTSzH_ntz1nDzsMl8VoaaqLkww499q3rWHlyC6Fkjd7fSyxsO'

function pushNotification(deviceIds, data) {
    data.timestamp = Date.now();
    var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
        to: deviceIds,
        data: data
    };

    return new Promise(function (resolve, reject) {
        fcm.send(message, function (error, response) {
            if (error) {
                console.log(err)
                reject(error);
            }
            else {
                console.log(response)
                resolve(response);
            }
        });
    });
}

// ========================== Export Module Start ==========================
export default {
    pushNotification
}
// ========================== Export Module End ============================