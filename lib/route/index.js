import responseHandler from '../responseHandler.js'
import basicAuth from '../middleware/basicAuth.js'

import v1Route from '../module/v1/route.js'

function route(app) {

    // Attach V1 Routes
    // app.use(basicAuth.basicAuthentication);

    app.use('/api/v1', v1Route);
    // Attach ErrorHandler to Handle All Errors
    app.use(responseHandler.handleError);

}

export default route