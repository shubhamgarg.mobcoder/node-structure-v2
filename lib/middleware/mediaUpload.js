import fs from 'fs'
import _ from 'lodash'

import loggerh from '../logger/index.js'
const logger = loggerh.logger

// import AWS from 'aws-sdk'
import { S3Client, PutObjectCommand } from '@aws-sdk/client-s3'

const s3 = new S3Client({
	region: config.cfg.s3.region,
	credentials: {
		accessKeyId: config.cfg.iamUser.keyId,
		secretAccessKey: config.cfg.iamUser.accessKey
	},
	signatureVersion: config.cfg.s3.signatureVersion
});

import config from '../config/index.js'
var Bucket = config.cfg.s3.bucketName;

function _fetchFilesFromReq(request) {

	if (request.file) {
		return [request.file];
	}
	else if (request.files) {
		return request.files;
	}
	else {
		//No Data
	}
}

function __deleteFiles(filePathList) {
	try {
		filePathList.map((file) => {
			fs.unlinkSync(file)
		})
	}
	catch (err) {
		logger.error(err)
	}
}

function uploadSingleMediaToS3() {

	return function (request, response, next) {
		var files = _fetchFilesFromReq(request);
		if (!files) {
			return next();
		}

		var file = files[0];
		var params = {
			Bucket: Bucket,
			Key: String(file.filename),
			Body: fs.createReadStream(file.path),

		};

		const command = new PutObjectCommand(params);

		return new Promise(function (resolve, reject) {

			s3.send(command, (error, data) => {
				if (data) {
					const locationUrl = `https://${params.Bucket}.s3.amazonaws.com/${params.Key}`;

					request.body.location = locationUrl;
					resolve;
				}
				else {
					logger.error(error)
					reject;
				}

				__deleteFiles(_.map(files, 'path'));

				return next();
			});
		});
	}
}

function uploadMultipleMediaToS3() {
	return function (request, response, next) {
		var files = _fetchFilesFromReq(request);
		if (!files) {
			return next();
		}

		const location = []
		files.map((files) => {
			let file = []
			file.push(files)
			var params = {
				Bucket: Bucket,
				Key: String(file.filename),
				Body: fs.createReadStream(file.path),

			};

			const command = new PutObjectCommand(params);

			return new Promise(function (resolve, reject) {

				s3.send(command, (error, data) => {
					if (data) {
						const locationUrl = `https://${params.Bucket}.s3.amazonaws.com/${params.Key}`;

						location.push(locationUrl)
						resolve;
					}
					else {
						logger.error(error)
						reject;
					}

					__deleteFiles(_.map(files, 'path'));
				})
				request.body.location = location;
				return next();

			});
		})
	}

}


// ========================== Export Module Start ==========================
export default {
	uploadSingleMediaToS3,
	uploadMultipleMediaToS3
}
// ========================== Export Module End ============================