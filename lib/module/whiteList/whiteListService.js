"use strict";

//========================== Load Modules Start =======================

//========================== Load internal modules ====================
// Load user dao
import _ from 'lodash'
import whiteListDao from "./whiteListDao.js"

//========================== Load Modules End ==============================================

function createIP(param) {
    return whiteListDao.createIP(param)
}

function create(param) {
    return whiteListDao.create(param)
}

function getByKey(param) {
    return whiteListDao.getByKey(param)
}

function update(query, update) {
    return whiteListDao.update(query, update)
}

function updateOne(query, update) {
    return whiteListDao.updateOne(query, update)
}

function getAllwhiteListIP(param) {
    return whiteListDao.getAllwhiteListIP(param)
}

//========================== Export Module Start ==============================

export default {
    createIP,
    create,
    getByKey,
    update,
    updateOne,
    getAllwhiteListIP
};

//========================== Export Module End ===============================
