"use strict";
//========================== Load Modules Start =======================

//========================== Load internal modules ====================
import mongoose from "mongoose";
import _ from 'lodash'
//========================== Load internal modules ====================
import whiteListModel from './whiteListModel.js'

// init user dao
import BaseDao from "../../dao/baseDao.js";
const whiteListDao = new BaseDao(whiteListModel);


//========================== Load Modules End ==============================================

function createIP(params) {
    let query = { IP: params.IP };
    let update = params;
    let option = {};
    option.new = true;
    option.upsert = true;
    return whiteListDao.updateOne(query, update, option);
}

function create(params) {
    let match = new messageModel(params);
    return whiteListDao.save(match);
}

function update(query, update) {
    update.updated = new Date();
    let option = {};
    option.new = true;
    return whiteListDao.update(query, update, option);
}

function updateOne(query, update) {
    update.updated = new Date();
    let option = {};
    option.new = true;
    return whiteListDao.updateOne(query, update, option);
}

function getByKey(query) {
    return whiteListDao.findOne(query)
}

function getAllwhiteListIP(param) {
    return whiteListDao.find()
}

//========================== Export Module Start ==============================

export default {
    createIP,
    create,
    update,
    updateOne,
    getByKey,
    getAllwhiteListIP
};

//========================== Export Module End ===============================
