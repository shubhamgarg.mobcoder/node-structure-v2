import express from 'express'
var router = express.Router();

import userRoute from './user/userRoute.js'
import adminRouter from './admin/adminRoute.js';

//========================== Export Module Start ==========================

//API version 1
router.use('/user', userRoute);
router.use('/admin', adminRouter)

export default router;
//========================== Export Module End ============================