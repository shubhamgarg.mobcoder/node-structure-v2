"use strict";
//========================== Load Modules Start =======================

//========================== Load internal modules ====================
import mongoose from "mongoose";
import _ from 'lodash'
//========================== Load internal modules ====================
import userModel from './userModel.js'

// init user dao
import BaseDao from '../../../dao/baseDao.js'
const userDao = new BaseDao(userModel);
import fcm from '../../../service/pushNotification.js'

//========================== Load Modules End ==============================================

async function isEmailExist(email) {
    try {
        return await userDao.findOne({ email: email })
    }
    catch (err) {
        return err
    }
}

async function createUser(params) {
    let query = {};

    query.name = params.name,
        query.email = params.email,
        query.password = params.password,
        query.username = params.username,
        query.userType = params.userType,
        query.gender = params.gender,
        query.dob = params.dob,
        query.designation = params.designation,
        query.companyName = params.companyName,
        query.phoneNo = params.phoneNo,
        query.clientIp = params.clientIp
    query.profileImage = params.profileImage

    return await userDao.save(query)
}

async function deleteUser(email) {
    return await userDao.deleteOne({ email: email })
}

async function verifyEmail(params) {
    const status = 2
    const query = { status: status }

    return await userDao.findOneAndUpdate({ email: params.email }, query)
}

async function changePassword(params) {
    return await userDao.findOneAndUpdate({ email: params.email }, { password: params.password })
}

async function isSocialIdExist(id) {
    return await userDao.findOne({ 'socialProfile.id': id })
}

async function socialSignup(params) {

    let query = {};

    query.email = params.email
    query.name = params.name
    query.socialProfile = { isSocial: true, socialType: params.socialType, id: params.id }

    return await userDao.save(query)
}

function getByKey(query) {
    return userDao.findOne(query)
}


//======================== Export Module Start ==============================

export default {
    createUser,
    verifyEmail,
    changePassword,
    getByKey,
    socialSignup,
    isEmailExist,
    deleteUser,
    isSocialIdExist
};

//========================== Export Module End ===============================
