/**
 * This file will have request and response object mappings.
 */
import _ from "lodash"
import contstants from "../../../constant.js"

function loginMapping(params, accessToken) {
    let respObj = {
        name: params.name,
        email: params.email,
        username: params.username,
    };

    if (params.gender) {
        respObj.gender = params.gender
    }
    if (params.dob) {
        respObj.dob = params.dob
    }
    if (params.designation) {
        respObj.designation = params.designation
    }
    if (params.companyName) {
        respObj.companyName = params.companyName
    }
    if (params.profileImage) {
        respObj.profileImage = params.profileImage
    }
    if (params.phoneNo) {
        respObj.phoneNo = params.phoneNo
    }
    if (params.deviceToken) {
        respObj.deviceToken = params.deviceToken
    }
    if (params.deviceID) {
        respObj.deviceID = params.deviceID
    }
    if (params.deviceTypeId) {
        respObj.deviceTypeId = params.deviceTypeId
    }
    if (params.profileImage) {
        respObj.profileImage = params.profileImage
    }

    return {
        "message": contstants.MESSAGES.USER_LOGIN,
        "accessToken": accessToken,
        "result": respObj
    }
}

function createMapping(params) {

    let respObj = {
        name: params.name,
        email: params.email,
        username: params.username,
    };

    if (params.gender) {
        respObj.gender = params.gender
    }
    if (params.dob) {
        respObj.dob = params.dob
    }
    if (params.designation) {
        respObj.designation = params.designation
    }
    if (params.companyName) {
        respObj.companyName = params.companyName
    }
    if (params.profileImage) {
        respObj.profileImage = params.profileImage
    }
    if (params.phoneNo) {
        respObj.phoneNo = params.phoneNo
    }
    if (params.deviceToken) {
        respObj.deviceToken = params.deviceToken
    }
    if (params.deviceID) {
        respObj.deviceID = params.deviceID
    }
    if (params.deviceTypeId) {
        respObj.deviceTypeId = params.deviceTypeId
    }

    return {
        "message": contstants.MESSAGES.OTP_SEND,
        'result': respObj
    }
}

function otpResend(email) {
    return {
        message: contstants.MESSAGES.OTP_SEND,
        email: email
    }
}

function changePassword(params) {
    return {
        message: contstants.MESSAGES.PASSWORD_CHANGED,
        email: params.email
    }
}

function logout() {
    return {
        message: contstants.MESSAGES.LOGOUT
    }
}

function deleteAccount() {

    return {
        message: contstants.MESSAGES.ACCOUNT_DELETED
    }
}

function socialSignup(params) {
    return {
        msg: contstants.MESSAGES.USER_LOGIN
    }
}

export default {
    loginMapping,
    otpResend,
    logout,
    deleteAccount,
    createMapping,
    changePassword,
    socialSignup
}