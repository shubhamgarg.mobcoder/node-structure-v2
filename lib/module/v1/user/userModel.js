// Importing mongoose
import mongoose from "mongoose";
import constants from "../../../constant.js";

var Schema = mongoose.Schema;
var User;

var UserSchema = new Schema({
    name: {
        type: String,
    },
    email: {
        type: String,
        index: true,
        unique: true,
    },
    password: {
        type: String
    },
    username: {
        type: String,
    },
    userType: {
        type: Number,
        default: 1, //1-user, 2-company
    },
    socialProfile: {
        isSocial: {
            type: Boolean,
            default: false,
        },
        socialType: {
            type: String,
        },
        id: {
            type: String
        }
    },
    gender: {
        type: Number,
        default: 0, //0 Undefined, 1 Male, 2 Female, 3 Others
        min: 0,
        max: 3
    },
    dob: {
        type: String,
    },
    designation: {
        type: String,
    },
    companyName: {
        type: String,
    },
    profileImage: {
        type: String,
    },
    phoneNo: {
        type: String,
    },
    deviceToken: {
        type: String
    },
    deviceID: {
        type: String,
    },
    deviceTypeId: {
        type: Number,
        default: 1, //1 iOS , 2 android , 3 web
        min: 1,
        max: 3
    },
    status: {
        type: Number, // 1: unverified, 2:verified
        default: 1
    },
    profileImage: {
        type: String
    },
    deleteInfo: {
        isDelete: {
            type: Boolean,
            default: false,
        },
        deleteDate: {
            type: Date,
        },
    }
},
    {
        timestamps: true
    }
);

//Export user module
User = mongoose.model(constants.DB_MODEL_REF.USER, UserSchema);

export default User


