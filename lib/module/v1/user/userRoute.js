import express from "express"
var userRouter = express.Router();
import requestIp from 'request-ip'

import resHndlr from "../../../responseHandler.js"
import middleware from "../../../middleware/index.js"
import userFacade from "./userFacade.js"
import validators from "./userValidators.js"

//==============================================================

userRouter.route("/signup")
    .post(middleware.multer.single('profileImage'), middleware.mediaUpload.uploadSingleMediaToS3('profile'), async function (req, res) {
        try {
            let profileImage;
            let profileImageData;

            let {
                name,
                email,
                password,
                username,
                userType,
                gender,
                dob,
                designation,
                companyName,
                phoneNo,
            } = req.body;

            let { user } = req;

            if (req.body.location) {
                profileImageData = req.file;
                profileImage = req.body.location;
            }

            let clientIp = requestIp.getClientIp(req);

            const result = await userFacade.create({
                name,
                email,
                password,
                username,
                userType,
                gender,
                dob,
                designation,
                companyName,
                phoneNo,
                clientIp,
                profileImage,
                profileImageData
            })
            console.log(result, '-123456789')
            resHndlr.sendSuccess(res, result, req);
        }
        catch (err) {
            resHndlr.sendError(res, err, req);
        }
    })

userRouter.route("/verifyEmail")
    .put(async function (req, res) {
        try {
            let { email, otp } = req.query;

            let clientIp = requestIp.getClientIp(req);

            const result = await userFacade.verifyEmail({ email, otp, clientIp })

            resHndlr.sendSuccess(res, result, req);
        }
        catch (err) {
            resHndlr.sendError(res, err, req);
        }
    });

userRouter.route('/resendOtp')
    .post(async function (req, res) {
        try {
            const { email } = req.body

            const result = await userFacade.otpResend(email)

            resHndlr.sendSuccess(res, result, req);
        }
        catch (err) {
            resHndlr.sendError(res, err, req);
        }

    })

userRouter.route("/login")
    .post(async function (req, res) {
        try {

            let { email, password } = req.body;

            let clientIp = requestIp.getClientIp(req);

            const result = await userFacade.userLogin({ email, password, clientIp })

            resHndlr.sendSuccess(res, result, req);
        }
        catch (err) {
            resHndlr.sendError(res, err, req);
        }
    });

userRouter.route("/socialSignup")
    .post(async function (req, res) {
        try {

            let { socialType, email, name, id, profilePicURL } = req.body;
            let clientIp = requestIp.getClientIp(req);

            const result = await userFacade.socialSignup({ socialType, email, name, id, profilePicURL, clientIp })

            resHndlr.sendSuccess(res, result, req);
        }
        catch (err) {
            resHndlr.sendError(res, err, req);
        }
    });

userRouter.route("/forgot")
    .post(async function (req, res) {
        try {
            let { email } = req.body;

            let clientIp = requestIp.getClientIp(req);

            const result = await userFacade.forgetPassword({ email, clientIp })

            resHndlr.sendSuccess(res, result, req);
        }
        catch (err) {

            resHndlr.sendError(res, err, req);
        }
    });


userRouter.route("/reset")
    .post(async function (req, res) {
        try {
            let { email, otp, password } = req.body;

            let clientIp = requestIp.getClientIp(req);

            const result = await userFacade.resetPassword({ email, otp, password, clientIp })

            resHndlr.sendSuccess(res, result, req);
        }
        catch (err) {
            resHndlr.sendError(res, err, req);
        }
    });

userRouter.route("/logout")
    .post([middleware.authenticate.autntctTkn], [middleware.authenticate.expireToken], async function (req, res) {
        try {

            const result = await userFacade.logout()

            resHndlr.sendSuccess(res, result, req);
        }
        catch (err) {
            resHndlr.sendError(res, err, req);
        }
    });

userRouter.route("/deleteAccount")
    .delete([middleware.authenticate.autntctTkn], [middleware.authenticate.expireToken], async function (req, res) {
        try {
            const { email } = req.query

            const result = await userFacade.deleteAccount(email)

            resHndlr.sendSuccess(res, result, req);
        }
        catch (err) {
            resHndlr.sendError(res, err, req);
        }
    });


export default userRouter;
